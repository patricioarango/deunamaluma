import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from 'firebase/app';

const styles1 = {
    height: '35px',
    marginTop: '7px',
    border: '2px solid #e91e63'
};

class Header extends Component {

firebaseLogout = () => {
	console.log("out");
	firebase.auth().signOut();
	window.location.reload();
}

 render() {
 	//console.log(this.props.user);
 	const nombre = this.props.nombre;
 	const foto = this.props.foto;
    return (
		  <header>
		    <nav className="cyan">
		    <div className="nav-wrapper">
		    <a data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a>
		      <li className="brand-logo great_vibes center">De una, Maluma</li>
		      <ul className="left hide-on-med-and-down">
		        <li><Link to='/'>Home</Link></li>
		        <li><Link to='/nuevo'>Agregar Lugar</Link></li>
		        <li><Link to='/ranking'>Ranking</Link></li>
		        { (nombre) ? <li><Link to='/ranking'>Mis Lugares</Link></li> : ""}
		      </ul>
		      	{ (nombre) ?
		      	<ul className="right">
		      		<li style={{marginRight: '4px'}}>{nombre}</li>
		      		<li><div className="valign-wrapper"><img style={styles1} src={foto} className="circle hide-on-small-only" /></div></li>
		      		<li><a onClick={(e) => {e.preventDefault(); this.firebaseLogout()}}><i className="material-icons">power_settings_new</i></a></li>
		      	</ul>  
		      	:
		      	<ul className="right">
		      		<li><Link to='/login'>Log In</Link></li>
		      		<li><a href=""><i className="material-icons">vpn_key</i></a></li>
		      	</ul>  
		      	}
		     </div> 	  
		    </nav>
		    <ul className="sidenav" id="mobile-demo">
			    <li><a href="sass.html">Sass</a></li>
			    <li><a href="badges.html">Components</a></li>
			    <li><a href="collapsible.html">Javascript</a></li>
		    	<li><a href="mobile.html">Mobile</a></li>
		    </ul>
		  </header>
		);
	}
}
export default Header;