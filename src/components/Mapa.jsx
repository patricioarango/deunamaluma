import React, {Component} from "react";
import { renderToString } from 'react-dom/server';
import { Link } from 'react-router-dom';
import GoogleMapReact from 'google-map-react';
import firebase from 'firebase'; 
import 'firebase/firestore';
import LugarInfo from './LugarInfo';
import Voto from './Voto';

const MY_API_KEY = "AIzaSyAC0uRicO3Rdrt7j4tALPexncxJ4S58DP0"
const myLatLng = {lat: -34.6125558,lng: -58.4067680999999};
const myLatLng2 = {lat: -34.6139682,lng: -58.37364979999999};
const markers = [];

class Mapa extends Component {
  static defaultProps = {
    center: {
      lat: -34.614715,
      lng:  -58.4029135
    },
    zoom: 14
  };

renderMarkers = (map,maps) => {
      firebase.firestore().collection('locaciones').onSnapshot((querySnapshot) => { 
      //console.log(querySnapshot);
      let marcadores = querySnapshot.docChanges();
      marcadores.map((change) => {
            if (change.type === "added") {
                let marcador = change.doc.data();
                let marcador_id = change.doc.id;
                if (marcador.lat !== "" && marcador.lng !== ""){
                let lugar = {
                  lugarId: marcador_id,
                  title: marcador.title,
                  direccion: marcador.direccion,
                  cantidad_votos: marcador.cantidad_votos,
                  promedio_votos: marcador.promedio_votos
                }
                /*let content = renderToString(<div className="row flow-text">
                                              <LugarInfo lugar={lugar} />
                                                {(this.props.user) ? <Voto lugarId={marcador_id}/> : ""}
                                                <Link className="waves-effect waves-light btn right yellow brown-text center" to={"ampliado/" + marcador_id}>Ver reseñas</Link>
                                              </div>);*/
                //https://stackoverflow.com/questions/39481484/how-to-use-a-reactjs-component-as-content-string-for-google-maps-api-infowindow
                let content = renderToString(<div className="row flow-text">
                                              <LugarInfo lugar={lugar} />
                                              <a className="waves-effect waves-light btn right yellow brown-text center" href={"ampliado/" + marcador_id}>Ver reseñas</a>
                                              </div>);

                  let marker = new maps.Marker({
                    id: marcador_id,
                    position: {lat: parseFloat(marcador.lat), lng: parseFloat(marcador.lng)},
                    map: map,
                    title: marcador.title,
                    zIndex: 2,
                    contentString: content
                  });
                
                  let infowindow = new maps.InfoWindow();
                  marker.addListener('click', function() {
                    infowindow.setContent(this.contentString);
                    infowindow.open(map, this);
                    map.setCenter(this.getPosition());
                  });

                  //mantenemos un array/objeto de markers para updatear las lat lng
                  markers[marcador_id] = marker;  
                }
            }
            if (change.type === "modified") {
                let marcador = change.doc.data();
                let marcador_id = change.doc.id;
                if (marcador.lat !== "" && marcador.lng !== ""){
                  markers[marcador_id].setPosition({lat: parseFloat(marcador.lat), lng: parseFloat(marcador.lng)});
                }
            }
            if (change.type === "removed") {
                console.log("Removed Marker: ", change.doc.data());
            }
      })
      querySnapshot.docs.map(function (documentSnapshot,i) {
      //let marcador = documentSnapshot.data();

      //infowindow
      /*marker.addListener('click', function() {
        infowindow.setContent(this.contentString);
        infowindow.open(map, this);
        map.setCenter(this.getPosition());
      });*/
    });

  });
}
/*renderMarkers = (map, maps) => {
  let marker = new maps.Marker({
    position: myLatLng,
    map,
    title: 'Hello World!'
  });
  let marker2 = new maps.Marker({
    position: myLatLng2,
    map,
    title: 'Hello World!23'
  });
}*/

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: 'calc(100vh - 64px)', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: MY_API_KEY }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
               
        >

        </GoogleMapReact>
      </div>
    );
  }
}

export default Mapa;