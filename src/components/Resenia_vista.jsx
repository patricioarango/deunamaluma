import React, { Component } from 'react';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Resenia_vista extends Component {
    constructor(props){
      super(props);
    }

  render() {
    return (  
        <div className="row">
          <div className="col s12">
            <div className="card blue-grey darken-1">
              <div className="card-content white-text">
                <span className="card-title">{this.props.resenia.user_name}</span>
                <p>{this.props.resenia.resenia}</p>
              </div>
            </div>
          </div>
        </div>
     )}
}
export default Resenia_vista;
