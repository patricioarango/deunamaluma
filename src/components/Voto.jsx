import React, { Component } from 'react';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Voto extends Component {

    constructor(props){
      super(props);
      this.state = {
                    puntaje: 0,
                  };
    }

puntuar = (puntos) => {
  const siguiente_punto = this.calcular_siguiente_puntos(puntos);
  //calcular_tamanio_estrella($(this),siguiente_punto);
  this.setState({puntaje: siguiente_punto});

    firebase.firestore().collection('locaciones').doc(this.props.lugarId).collection('votos').doc(this.props.user.uid).set({
      lugarId: this.props.lugarId,
      uid: this.props.user.uid,
      voto: parseInt(siguiente_punto)
    }); 
}

calcular_siguiente_puntos = (punto) => {
  if (punto == 5){
    punto = 0;
  } else if (punto == 0){
    punto = parseInt(1);
  }
  else {
    punto = parseInt(punto) + 1;
  }
  return punto;
}

  render() {
    return (  
      <div className="input-field col s12">
          <div className="input-field col s6">
            <i className="material-icons prefix">star</i>
            <input id="puntaje" name="puntaje" type="text" value={this.state.puntaje} readOnly/>
            <label htmlFor="puntaje" className="active">tu voto</label>
          </div>  
         <div className="input-field col s6">
            <a className="btn btn-floating btn-large pulse" onClick={() => {this.puntuar(this.state.puntaje)}}><i className="material-icons">star</i></a>
          </div>
      </div>            
     )}
}
export default Voto;
