import React, {Component} from 'react';
import firebase from 'firebase/app';
import { Redirect } from 'react-router-dom';

class Login extends Component {

state = {
  redirect: false
}


firebase_login = () => {
  var provider = new firebase.auth.GoogleAuthProvider(); 
  firebase.auth().getRedirectResult().then((result) => {
    if (result.credential) {
      console.log(result.credential);
      this.setState({ redirect: true });
  } else {
    firebase.auth().signInWithRedirect(provider);
  }
}).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    // ...
});
}
	
  render() {
    const redirect = (this.props.user) ? true : false;
    if (redirect) {
       return <Redirect to='/'/>;
     }
    return (	
      <div className="center">
        <p>Google Log In</p>
        <button onClick={() => {this.firebase_login()}} className="btn btn-floating pulse pink">
                    <i className="material-icons right">send</i>
                </button>
      </div>
    )}
}

export default Login;