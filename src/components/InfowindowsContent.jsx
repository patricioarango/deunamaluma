import React, { Component } from 'react';
import LugarInfo from './LugarInfo';
import Voto from './Voto';

class InfowindowsContent extends Component {

  render() {
    return (  
        <div>
          <LugarInfo lugar={this.props.lugar} />
          <a className="waves-effect waves-light btn right yellow brown-text" href={"/ampliado/" + this.props.lugarId}>Ver reseñas</a>
          <div className="row">
          <Voto lugarId={this.props.lugar.lugarId} user={this.props.user} />
          </div>
        </div>
     )}
}
export default InfowindowsContent;
