import React, { Component } from 'react';

class LugarInfo extends Component {

  render() {
    return (  
        <div className="flow-text">
          <h4>{this.props.lugar.title}</h4> 
          <h6 className="grey-text">{this.props.lugar.direccion}</h6>
          <p>cantidad de votos: {this.props.lugar.cantidad_votos}</p>
          <p>puntaje votos: {this.props.lugar.puntaje_votos}</p>
          <p>promedio votos: {this.props.lugar.promedio_votos}</p>
        </div>   
     )}
}
export default LugarInfo;
