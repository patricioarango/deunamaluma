import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Resenia from './Resenia';
import Resenia_vista from './Resenia_vista';
import Voto from './Voto';
import firebase from 'firebase'; 
import 'firebase/firestore';
import LugarInfo from './LugarInfo';

class Ampliado extends Component {

    constructor(props){
      super(props);
      this.state = {
        lugarId: this.props.match.params.lugarId,
        user_resenia: "",
        user_voto: 0,
        marcador: [],
        resenias: [],
        votos: [],
      }
    }

componentWillMount() {
    firebase.firestore().collection('locaciones').doc(this.props.match.params.lugarId).onSnapshot((doc) => {
      this.setState({marcador: doc.data()});
    });

    firebase.firestore().collection('locaciones').doc(this.props.match.params.lugarId).collection("resenias").onSnapshot((querySnapshot) => {
        querySnapshot.docs.map((resenia) => {
          this.setState(prevState => ({resenias: [...prevState.resenias, resenia.data()]}) );
        });
    });
}

  render() { 
    const listado_resenias = this.state.resenias.map((item, i) => {
      return <Resenia_vista key={i} resenia={item} /> 
    });

    return (  
        <div className="container">
            <div className="col s12">
              <div className="flow flow-text">
                <h3><Link to="/"><i style={{cursor:'pointer'}} className="material-icons right medium cyan-text">clear</i></Link></h3>
                <h3><LugarInfo lugar={this.state.marcador} /></h3>
              </div>
            </div>  
            <div>
              <h3>Todas las reseñas</h3>
              {listado_resenias}
            </div>

          </div> /* cierre del div container */  
     )}
}
export default Ampliado;
