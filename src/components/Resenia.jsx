import React, { Component } from 'react';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Resenia extends Component {

    constructor(props){
      super(props);
      this.state = {
                    resenia: ''
                  };
    }

handleResenia = (event) => {
  const resenia = event.target.value;
    this.setState({resenia: resenia}, () => {
    firebase.firestore().collection('locaciones').doc(this.props.lugarId).collection('resenias').doc(this.props.user.uid).set({
          lugarId: this.props.lugarId,
          uid: this.props.user.uid,
          user_name: this.props.user.displayName,
          resenia: this.state.resenia
        }); 
    });   
}
  render() {
    console.log("this.props.user_resenia");
    console.log(this.props.user_resenia);
    return (  
        <div className="input-field col s12">
          <i className="material-icons prefix">note_add</i>
          <textarea id="textarea1" className="materialize-textarea" onChange={this.handleResenia}>{this.props.user_resenia}</textarea>
          <label htmlFor="textarea1">Reseñalo!</label>
        </div>
     )}
}
export default Resenia;
