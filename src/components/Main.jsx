import React, {Component} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import firebase from 'firebase/app';
import Header from './Header';
import Mapa from './Mapa';
import Login from './Login';
import Nuevo from './Nuevo';
import Actividad from './Actividad';
import Ampliado from './Ampliado';
import Ranking from './Ranking';

  var config = {
    apiKey: "AIzaSyB3SebNE2TtVDbsyCOpbYfqwO0f0Jc18gQ",
    authDomain: "deunamaluma.firebaseapp.com",
    databaseURL: "https://deunamaluma.firebaseio.com",
    projectId: "deunamaluma",
    storageBucket: "deunamaluma.appspot.com",
    messagingSenderId: "538439619396"
  };
  const app = firebase.initializeApp(config);

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {user: null}
    this.state["nombre"] = null;
    this.state["foto"] = null;
  }


componentWillMount(){
		//acá chequeo firebase si está logueado o no. 
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user.displayName);
        console.log("user state => logued in");
        this.setState({user: user, nombre: user.displayName,foto: user.photoURL});
      } else {
        console.log("user state => logued out");
        this.setState({user: null});
      }
    }); 		
		//si el componente es privado y no está logueado, ir a loguin
		//si: paso el user al state y puede ver componentes privados
		//no: puede ver componentes publicos.
}
	
  render() {
    return (	
      <main>
      <Header user={this.state.user} nombre={this.state.nombre} foto={this.state.foto} />
        <Switch>
          <Route exact path='/' render={() => <Mapa user={this.state.user} />} />
          <Route exact path='/login' render={(props) => <Login user={this.state.user} someProp="2" />} />
          <Route exact path='/ampliado/:lugarId' render={(props) => <Ampliado {...props} user={this.state.user} />} />
          <Route exact path='/ranking' render={(props) => <Ranking {...props} user={this.state.user} />} />
          <PrivateRoute exact path='/nuevo' user={this.state.user} component={Nuevo} />
          <PrivateRoute exact path='/actividad' user={this.state.user} component={Actividad} />
          </Switch>
      </main>
    )}
}

const PrivateRoute = ({ component: Component,user,...rest }) => (
    
  <Route {...rest} render={(props) => ( 
    (user) 
      ? <Component {...rest} user={user} />
      : <Redirect to={{
          pathname: '/login'
        }} />
  )} />
)

export default Main;