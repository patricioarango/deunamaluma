import React, {Component} from "react";
import { Link } from 'react-router-dom';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Ranking extends Component {
      constructor(props){
      super(props);
      this.state = {
        marcadores: [],
      }
    }

componentWillMount() {
    firebase.firestore().collection('locaciones').orderBy("cantidad_votos","desc").get().then((querySnapshot) => { 
    querySnapshot.docs.map((marcador) => {
      this.setState(prevState => ({marcadores:[...prevState.marcadores, marcador.data()]}) );
    })
  });
}

  render() {    
    const list = this.state.marcadores.map((item, i) => {
      return <tr key={i}>
              <td>{item.title}</td>
              <td>{item.direccion}</td>
              <td>{item.cantidad_votos}</td>
              <td>{item.promedio_votos}</td>
              <td><Link className="waves-effect waves-light btn right yellow brown-text center" to={"ampliado/" + item.lugarId}>reseñar /votar</Link></td>
            </tr>
    });

    return (
      <div className='row'>
          <h3>Ranking</h3>
          
        <table className="striped">
        <tbody>
        <tr>
          <th>Lugar</th>
          <th>Dirección</th>
          <th>Puntos</th>
          <th>Votos</th>
          <th>Ver</th>
        </tr>
        {list}
        </tbody></table>
      </div>
    );

  }
}
export default Ranking;