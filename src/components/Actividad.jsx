import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Actividad extends Component {
    constructor(props){
      super(props);
      this.state = {lugares: []};
    }

componentDidMount(){
    firebase.firestore().collection('locaciones').where("uid","==",this.props.user.uid).get()
    .then((querySnapshot) => { 
      const markers = [];
      querySnapshot.docs.map( documentSnapshot => {
        let marker;
        marker = documentSnapshot.data();
        marker["id"] = documentSnapshot.id;
        markers.push(marker);
      });
      this.setState({lugares: markers});
    });
}
render() {
      
        return (
            <div>
              <div className="row">
                <div className="col s12">
                  <div className="card-panel teal valign-wrapper">
                  <img id="user_photo" className="circle doble_borde" style={{width: "200px"}} src={this.props.user.photoURL} />
                  <h3 className="white-text">{this.props.user.displayName}</h3>
                  </div>
              </div>
            </div>
                {
                    this.state.lugares.map(lugar => <p>{lugar.title}</p>)
                }
            </div>
        );
      
 }
}
export default Actividad;
