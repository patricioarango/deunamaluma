import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Mapa from './Mapa';
import Resenia from './Resenia';
import Voto from './Voto';
import firebase from 'firebase'; 
import 'firebase/firestore';

class Nuevo extends Component {

    constructor(props){
      super(props);
      this.state = {
                    direccion: '',
                    validacionStatus: null,
                    direccionGMaps: '',
                    nombre: '',
                    lugarId: ''
                  };
    }

componentDidMount(){
    //obtenemos el ID del lugar para luego actualizarla
    firebase.firestore().collection('locaciones').add({}).then((snap) => {
     this.setState({lugarId: snap.id})
    }); 
}

handleDireccion = (event) => {
    const direccion = event.target.value;
    //setState es async así que hay que ir a firebase en el then de SetState
    this.setState({direccion: event.target.value}, () => {
      const url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
      const address = this.state.direccion.replace(/ /g, "+");
      const city = "+Buenos+Aires,+Argentina";
      const key = "&key=AIzaSyAC0uRicO3Rdrt7j4tALPexncxJ4S58DP0";
      const final_url = url + address + city + key;
      fetch(final_url).then(response => response.json()).then((data) => {
        if (data.status === 'OK'){
            this.setState({direccionGMaps: data.results[0].formatted_address});
            //data.results[0].geometry.location
            this.guardar_direccion_firebase(data.results[0]);
        }
      });
  });  
}

guardar_direccion_firebase = (datos) => {
  if (datos.geometry.location.lat != "" && datos.geometry.location.lng != ""){
    let direccion_formateada = datos.formatted_address.split(",",1);
    firebase.firestore().collection('locaciones').doc(this.state.lugarId).set({
      lugarId: this.state.lugarId,
      lat: datos.geometry.location.lat,
      lng: datos.geometry.location.lng,
      direccion: direccion_formateada[0],
      title: this.state.nombre,
      cantidad_votos: 0,
      puntaje_votos: 0,
      promedio_votos: 0,
      uid: this.props.user.uid
    });
  }
}

handleNombre = (event) => {
  const nombre = event.target.value;
    this.setState({nombre: nombre}, () => {

    firebase.firestore().collection('locaciones').doc(this.state.lugarId).update({
          'title': this.state.nombre
        }); 
    });   
}

  render() {
    return (  
<div className="row">
    <div className="col s6">
      <Mapa/>
    </div>
    <div className="col s6">
      <div className="flow flow-text">
      <h3>¿Cuál es tu lugar secreto?<Link to="/"><i style={{cursor:'pointer'}} className="material-icons right medium cyan-text">clear</i></Link></h3>
          <div className="input-field col s12">
            <i className="material-icons prefix">search</i>
            <input id="direccion" type="text" value={this.state.direccion} className={this.state.validacionStatus} onChange={this.handleDireccion} />
            <label htmlFor="direccion">Buscá la dirección</label>
          </div>
          <div className="input-field col s12">
            <i className="material-icons prefix">room</i>
            <input name="direccionGMaps" type="text" value={this.state.direccionGMaps} readOnly />
            <label htmlFor="direccionGMaps" className="active">dirección real</label>
          </div>
          <div className="input-field col s12">
            <i className="material-icons prefix">restaurant</i>
            <input id="nombre" type="text" className="validate" onChange={this.handleNombre}/>
            <label htmlFor="nombre">Nombre</label>
          </div>
        <Resenia lugarId={this.state.lugarId} user={this.props.user} /> 
        <Voto lugarId={this.state.lugarId} user={this.props.user} /> 
      </div>
    </div>  
  </div> /* cierre del div container */

      )}
}
export default Nuevo;
