import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

export const votos =  functions.firestore
    .document('locaciones/{docId}/votos/{votId}')
    .onWrite((change, context) => {
      	// Get an object representing the document
      	// e.g. {'name': 'Marie', 'age': 66}
      	const newValue = change.after.data();
		return calcularCantidadVotos(newValue.lugarId);
    });

//contar la cantidad de votos del lugar
function calcularCantidadVotos(lugarId){
    return admin.firestore().collection('locaciones').doc(lugarId).collection('votos').get().then((querySnapshot) => {
    
        const cantidad_votos = querySnapshot.size;
        
        let total_votos = 0;
        querySnapshot.docs.map(voto => {
            total_votos = sumarVotos(total_votos,voto.data().voto);
        });

        return guardarValoresVotos(cantidad_votos,total_votos,lugarId);
    });
}
//updateamos los votos del lugar
function guardarValoresVotos(cantidad_votos,total_votos,lugarId){
    return admin.firestore().collection('locaciones').doc(lugarId).update({
        "cantidad_votos": cantidad_votos,
        "puntaje_votos": total_votos,
        "promedio_votos": total_votos/cantidad_votos
    });
}

function sumarVotos(total,voto){
    return total + voto;
}